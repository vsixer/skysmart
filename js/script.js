$(function ()
{
  let page = {
    init()
    {
      //region Title section visibility handlers
      $(window).on(
        'DOMContentLoaded load resize scroll',
        onElementEnterViewport(this.titleSection.element, () =>
        {
          this.titleSection.onEnterViewport();
        }),
      );

      $(window).on(
        'DOMContentLoaded load resize scroll',
        onElementLeaveViewport(this.titleSection.element, () =>
        {
          this.titleSection.onLeaveViewport();
        }),
      );
      //endregion

      //region Benefits section visibility handlers
      $(window).on(
        'DOMContentLoaded load resize scroll',
        onElementEnterViewport(this.benefitsSection.freeLesson.element, () =>
        {
          this.benefitsSection.freeLesson.onEnterViewport();
        }),
      );

      $(window).on(
        'DOMContentLoaded load resize scroll',
        onElementLeaveViewport(this.benefitsSection.freeLesson.element, () =>
        {
          this.benefitsSection.freeLesson.onLeaveViewport();
        }),
      );
      //endregion

      //region Preview section visibility handlers
      $(window).on(
        'DOMContentLoaded load resize scroll',
        onElementEnterViewport(this.previewSection.playerElement, () =>
        {
          this.previewSection.player.onEnterViewport();
        }),
      );

      $(window).on(
        'DOMContentLoaded load resize scroll',
        onElementLeaveViewport(this.previewSection.playerElement, () =>
        {
          this.previewSection.player.onLeaveViewport();
        }),
      );
      //endregion

      //region Heart section visibility handlers
      $(window).on(
        'DOMContentLoaded load resize scroll',
        onElementEnterViewport(this.heartSection.shapeElement, () =>
        {
          this.heartSection.onEnterViewport();
        }),
      );

      $(window).on(
        'DOMContentLoaded load resize scroll',
        onElementLeaveViewport(this.heartSection.shapeElement, () =>
        {
          this.heartSection.onLeaveViewport();
        }),
      );
      //endregion

      //region Subscribe section visibility handlers
      $(window).on(
        'DOMContentLoaded load resize scroll',
        onElementEnterViewport(this.subscribeSection.arrowElement, () =>
        {
          this.subscribeSection.onEnterViewport();
        }),
      );
      //endregion

      this.previewSection.init();
      this.subscribeSection.init();
    },
    headerSection: {
      onSubscribeBtnClick()
      {
        page.subscribeSection.scrollPageTillThisSection();
      },
    },
    titleSection: {
      element: $('.title-section'),
      stickerFirstElement: $('.title-section__sticker_first'),
      stickerSecondElement: $('.title-section__sticker_second'),
      /**
       * Event handler
       */
      onEnterViewport()
      {
        //region Show first sticker
        this.stickerFirstElement.addClass('title-section__sticker_visible');
        //endregion

        //region Show second sticker after delay
        setTimeout(() =>
        {
          this.stickerSecondElement.addClass('title-section__sticker_visible');
        }, 200);
        //endregion
      },
      /**
       * Event handler
       */
      onLeaveViewport()
      {
        //region Hide all stickers
        this.stickerFirstElement.removeClass('title-section__sticker_visible');
        this.stickerSecondElement.removeClass('title-section__sticker_visible');
        //endregion
      },
    },
    benefitsSection: {
      element: $('.benefits-section'),
      freeLesson: {
        element: $('.benefits-section__free-lesson'),
        arrowElement: $('.benefits-section__free-lesson-arrow'),
        /**
         * Event handler
         */
        onEnterViewport()
        {
          this.arrowElement.addClass('benefits-section__free-lesson-arrow_visible');
        },
        /**
         * Event handler
         */
        onLeaveViewport()
        {
          this.arrowElement.removeClass('benefits-section__free-lesson-arrow_visible');
        },
      },
      onCardClick()
      {
        page.subscribeSection.scrollPageTillThisSection();
      },
      onFreeLessonLinkClick(event)
      {
        event.preventDefault();

        page.subscribeSection.scrollPageTillThisSection();
      },
    },
    previewSection: {
      element: $('.preview-section'),
      player: {
        isInViewport: undefined,
        /**
         * Set src attribute of video player
         *
         * @param videoSrc
         */
        setSrc(videoSrc)
        {
          page.previewSection.playerElement.attr('src', videoSrc);
        },
        /**
         * Play video
         */
        play()
        {
          page.previewSection.playerElement.get(0).play();
        },
        /**
         * Pause video
         */
        pause()
        {
          page.previewSection.playerElement.get(0).pause();
        },
        /**
         * Mute video
         */
        mute()
        {
          page.previewSection.playerElement.prop('muted', true);
        },
        /**
         * Unmute video
         */
        unmute()
        {
          page.previewSection.playerElement.prop('muted', false);
        },
        /**
         * Event handler
         */
        onEnterViewport()
        {
          this.play();
        },
        /**
         * Event handler
         */
        onLeaveViewport()
        {
          this.pause();
        },
        onMuteIconClick(event)
        {
          if (! page.previewSection.playerElement.prop('muted'))
          {
            page.previewSection.player.mute();
            $(event.target).addClass('preview-section__player-audio_mute');
            return;
          }

          page.previewSection.player.unmute();
          $(event.target).removeClass('preview-section__player-audio_mute');
        },
      },
      playerElement: $('.preview-section__player'),
      selectorElements: $('.preview-section__selector'),
      /**
       * Constructor
       */
      init()
      {
        //region Set video url of first selector
        let videoUrl = this.selectorElements.first().data('video-url');

        this.player.setSrc(videoUrl);
        //endregion
      },
      /**
       * Event handler
       *
       * @param event
       */
      onSelectorClick(event)
      {
        //region Define event target
        let targetElement = $(event.target);
        //endregion

        //region Set new video url
        let videoUrl = targetElement.data('video-url');

        this.player.setSrc(videoUrl);
        //endregion

        //region Play video
        this.player.play();
        //endregion

        //region Visually change active selector
        let selectorActiveClass = 'preview-section__selector_active';

        this.selectorElements.removeClass(selectorActiveClass);
        targetElement.addClass(selectorActiveClass);
        //endregion
      },
    },
    heartSection: {
      element: $('.heart-section'),
      shapeElement: $('.heart-section__shape'),
      stickerFirstElement: $('.heart-section__sticker_first'),
      stickerSecondElement: $('.heart-section__sticker_second'),
      /**
       * Event handler
       */
      onEnterViewport()
      {
        //region Show heart shape
        this.shapeElement.addClass('heart-section__shape_visible');
        //endregion

        //region Show stickers after delay
        setTimeout(() =>
        {
          //region Show first sticker
          this.stickerFirstElement.addClass('heart-section__sticker_visible');
          //endregion

          //region Show second sticker after delay
          setTimeout(() =>
          {
            this.stickerSecondElement.addClass('heart-section__sticker_visible');
          }, 200);
          //endregion
        }, 300);
        //endregion
      },
      /**
       * Event handler
       */
      onLeaveViewport()
      {
        //region Hide heart shape
        this.shapeElement.removeClass('heart-section__shape_visible');
        //endregion

        //region Hide all stickers
        this.stickerFirstElement.removeClass('heart-section__sticker_visible');
        this.stickerSecondElement.removeClass('heart-section__sticker_visible');
        //endregion
      },
    },
    subscribeSection: {
      element: $('.subscribe-section'),
      arrowElement: $('.subscribe-section__arrow'),
      pizzaElement: $('.subscribe-section__pizza'),
      pizzaDropZoneElement: $('.subscribe-section__pizza-drop-zone'),
      stickerElement: $('.subscribe-section__sticker'),
      init()
      {
        this.pizzaElement.draggable({
          start: () =>
          {
            this.pizzaElement.removeClass('subscribe-section__pizza_aligned');
          },
        });

        this.pizzaDropZoneElement.droppable({
          accept: this.pizzaElement,
          drop: () =>
          {
            //region Align pizza to dropzone
            this.pizzaElement.addClass('subscribe-section__pizza_aligned');

            this.pizzaElement.css({
              top: this.pizzaDropZoneElement.get(0).offsetTop,
              left: this.pizzaDropZoneElement.get(0).offsetLeft,
            });
            //endregion

            //region Disable pizza draggable
            this.pizzaElement.draggable('disable');
            //endregion

            //region Hide arrow
            this.arrowElement.removeClass('subscribe-section__arrow_visible');
            //endregion

            //region Show sticker
            this.stickerElement.addClass('subscribe-section__sticker_visible');
            //endregion
          },
        });
      },
      /**
       * Scroll page to show this section
       */
      scrollPageTillThisSection()
      {
        $([document.documentElement, document.body]).animate({
          scrollTop: this.element.offset().top,
        }, 300);
      },
      /**
       * Event handler
       */
      onEnterViewport()
      {
        this.arrowElement.addClass('subscribe-section__arrow_visible');
      },
      /**
       * Event handler
       */
      onLeaveViewport()
      {
        this.arrowElement.removeClass('subscribe-section__arrow_visible');
      },
    },
  };

  page.init();

  window.page = page;
});


function isElementInViewport(el)
{
  if (typeof jQuery === 'function' && el instanceof jQuery)
  {
    el = el[0];
  }

  let rect = el.getBoundingClientRect();

  return (
    rect.top <= $(window).height() &&
    rect.bottom >= 0
  );
}


function onElementEnterViewport(el, callback)
{
  return function ()
  {
    let visibilityIndicationClass = 'in-viewport';
    let currentVisibilityState = el.hasClass(visibilityIndicationClass);
    let newVisibilityState = isElementInViewport(el);

    if (
      newVisibilityState !== currentVisibilityState
      && newVisibilityState
    )
    {
      el.addClass(visibilityIndicationClass);

      if (typeof callback == 'function')
      {
        callback();
      }
    }
  };
}


function onElementLeaveViewport(el, callback)
{
  return function ()
  {
    let visibilityIndicationClass = 'in-viewport';
    let currentVisibilityState = el.hasClass(visibilityIndicationClass);
    let newVisibilityState = isElementInViewport(el);

    if (
      newVisibilityState !== currentVisibilityState
      && ! newVisibilityState
    )
    {
      el.removeClass(visibilityIndicationClass);

      if (typeof callback == 'function')
      {
        callback();
      }
    }
  };
}
